# Videogame - Raining Cloud and Parallax effect #

Earlier prototype of this research project: https://bitbucket.org/Paulo-Jorge/test_grammar_patterns

Alternative version, more game oriented.

To run execute the file "main.py", and you need to have Python 2.7 installed and a version of Pyglet (just do "pip install pyglet" in the command line to get it). 

## Contacts:
My Homepage: [www.paulojorgepm.net](http://www.paulojorgepm.net)